resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

resource "google_compute_network" "vpc" {
  name                    = "${var.CLUSTER_NAME}-vpc"
  auto_create_subnetworks = "false"
//  project = var.gcp_project
}

# https://www.terraform.io/docs/providers/google/r/compute_subnetwork.html
resource "google_compute_subnetwork" "vpc_subnetwork" {
//  project = var.gcp_project
  # The name of the resource, provided by the client when initially creating
  # the resource. The name must be 1-63 characters long, and comply with
  # RFC1035. Specifically, the name must be 1-63 characters long and match the
  # regular expression [a-z]([-a-z0-9]*[a-z0-9])? which means the first
  # character must be a lowercase letter, and all following characters must be
  # a dash, lowercase letter, or digit, except the last character, which
  # cannot be a dash.
  #name = "default-${var.gcp_cluster_region}"
  name = "${google_compute_network.vpc.name}-subnetwork"



  ip_cidr_range = var.vpc_subnetwork_cidr_range

  # The network this subnet belongs to. Only networks that are in the
  # distributed mode can have subnetworks.
  network = google_compute_network.vpc.name

  # An array of configurations for secondary IP ranges for VM instances
  # contained in this subnetwork. The primary IP of such VM must belong to the
  # primary ipCidrRange of the subnetwork. The alias IPs may belong to either
  # primary or secondary ranges.
  secondary_ip_range = [
    {
      range_name    = var.cluster_secondary_range_name
      ip_cidr_range = var.cluster_secondary_range_cidr
    },
    {
      range_name    = var.services_secondary_range_name
      ip_cidr_range = var.services_secondary_range_cidr
    },
  ]

  # When enabled, VMs in this subnetwork without external IP addresses can
  # access Google APIs and services by using Private Google Access. This is
  # set explicitly to prevent Google's default from fighting with Terraform.
  private_ip_google_access = true
}