Techturbid Terraform Registry - gke_vpc
---
#### VPC module used by techturbid's GKE modules like gke_base and gke_minimal

By default this module will create a vpc and subnet for each cluster based on it's name.
 
It will also configure the same ip addressing to ease the management of GKE clusters across the organization

---

The input required for this modules are:

|            **Variable**           |       **Value**       |
|-----------------------------------|:----------------------|
| CLUSTER_NAME                      |                       |

--- 
 
The variables with default values are:

|             **Variable**           |       **Value**       |
|------------------------------------|:----------------------|
| vpc_subnetwork_cidr_range          | 10.0.16.0/**20**      |
| cluster_secondary_range_name       | pods                  |
| cluster_secondary_range_cidr       | 10.16.0.0/**12**      |
| services_secondary_range_name      | services              |
| services_secondary_range_cidr      | 10.1.0.0/**20**       |

---

This module will output the following values:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| vpc_network_name                   | google_compute_network.vpc.name               |
| vpc_subnetwork_name                | google_compute_subnetwork.vpc_subnetwork.name |
| cluster_secondary_range_name       | var.cluster_secondary_range_name              |
| services_secondary_range_name      | var.services_secondary_range_name             |

---

This module requires the following providers:
```hcl-terraform
provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}
```

---